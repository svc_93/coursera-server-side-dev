// Using Mongoose Models for Dishes schema

var mongoose = require('mongoose'),
  assert = require('assert');

var Dishes = require('./models/dishes');

// Connection URL
var url = 'mongodb://localhost:27017/conFusion';
mongoose.connect(url);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function(){
  // Connected to DB
  console.log("Connected correctly to the server");

  // create a new dish
  Dishes.create({
    name: 'Uthapizza',
    image: "images/uthapizza.png",
    category: "mains",
    label: "Hot",
    price: "4.99",
    description: 'Test',
    comments: [
      {
        rating: 5,
        comment: 'Imagine all the eatables, living in conFusion!',
        author: 'John Lemon'
      }
    ]
  }, function(err, dish){
    if(err) throw err;

    console.log('Dish created!');
    console.log(dish);
    var id = dish._id;

    // get all the dishes - 3 sec timeout between create and update
    setTimeout(function(){
      Dishes.findByIdAndUpdate(id, {
        $set: {
          description: 'A unique combination',
          price: "4.99"
        }
      }, {
        // new = true returns updated dish
        new: true
      })
      .exec(function(err,dish){
        if(err) throw err;
        console.log('Updated Dish!');
        console.log(dish);
        // prints currency with decimal places
        console.log("Currency " + dish.price.toFixed(2));

        // Adding new comments
        dish.comments.push({
          rating: 4,
          comment: 'Sends anyone to heaven',
          author: 'Matt Daemon'
        });

        dish.save(function(err,dish){
          console.log('Updated comments!');
          console.log(dish);

          db.collection('dishes').drop(function(result){
            db.close();
          });
        });
      });
    },3000);
  });
});
