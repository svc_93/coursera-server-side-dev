// Mongoose Model for Leadership

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a Schema
var leaderSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  image: {
    type: String,
    required: true,
  },
  designation: {
    type: String,
    required: true
  },
  abbr: {
    type: String
  },
  description: {
    type: String,
    required: true
  }
}, {
  // automatically adds createdAt and updatedAt values to the documents
  timestamps: true
});

// create a Model for the above schema
// Mongoose creates a collection in MongoDB with the plural of the model name specified here
var Leadership = mongoose.model('Leader', leaderSchema);

// make this model available to the Node application
module.exports = Leadership;
