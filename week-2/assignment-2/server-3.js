// Using Mongoose Models for Leadership schema

var mongoose = require('mongoose'),
  assert = require('assert');

var Leadership = require('./models/leadership');

// Connection URL
var url = 'mongodb://localhost:27017/conFusion';
mongoose.connect(url);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function(){
  // Connected to DB
  console.log("Connected correctly to the server");

  // create a new leader
  Leadership.create({
    name: 'Alberto Somayya',
    image: "images/alberto.png",
    designation: "Chief Epicurious Officer",
    abbr: "CEO",
    description: 'Test'
  }, function(err, leader){
    if(err) throw err;

    console.log('Leader created!');
    console.log(leader);
    var id = leader._id;

    // get all the leaders - 3 sec timeout between create and update
    setTimeout(function(){
      Leadership.findByIdAndUpdate(id, {
        $set: {
          description: 'Our CEO, Peter Pan'
        }
      }, {
        // new = true returns updated leader
        new: true
      })
      .exec(function(err,leader){
        if(err) throw err;
        console.log('Updated Leader!');
        console.log(leader);

        db.collection('leaders').drop(function(result){
          console.log(result);
          db.close();
        });
      });
    },3000);
  });
});
