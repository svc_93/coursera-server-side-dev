// Using Mongoose Models for Promotions schema

var mongoose = require('mongoose'),
  assert = require('assert');

var Promotions = require('./models/promotions');

// Connection URL
var url = 'mongodb://localhost:27017/conFusion';
mongoose.connect(url);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function(){
  // Connected to DB
  console.log("Connected correctly to the server");

  // create a new promotion
  Promotions.create({
    name: 'Weekend Grand Buffet',
    image: "images/buffet.png",
    label: "New",
    price: "19.99",
    description: 'Test'
  }, function(err, promotion){
    if(err) throw err;

    console.log('Promo created!');
    console.log(promotion);
    var id = promotion._id;

    // get all the promotions - 3 sec timeout between create and update
    setTimeout(function(){
      Promotions.findByIdAndUpdate(id, {
        $set: {
          description: 'Featured Dish'
        }
      }, {
        // new = true returns updated promotion
        new: true
      })
      .exec(function(err,promotion){
        if(err) throw err;
        console.log('Updated Promo!');
        console.log(promotion);

        db.collection('promotions').drop(function(result){
          console.log(result);
          db.close();
        });
      });
    },3000);
  });
});
