// MongoDBClient using operations.js to interact with Server
// Note the chaining of DB Operations - this is to ensure synchronous execution

var MongoClient = require('mongodb').MongoClient,
  assert = require('assert');

var dboperations = require('./operations');

// Connection URL
var url = 'mongodb://localhost:27017/conFusion';

// Use connect method to connect to the server
MongoClient.connect(url, function(err,db){
  assert.equal(err,null);
  console.log("Connected to the server");

  dboperations.insertDocument(db, {name: "Klingon", description: "Test"},
    "dishes", function(result){
      console.log(result.ops);

      dboperations.findDocuments(db, "dishes", function(docs){
        console.log(docs);

        dboperations.updateDocument(db, {name: "Klingon"}, {description: "Update Test"},
          "dishes", function(result){
            console.log(result.result);

            dboperations.findDocuments(db, "dishes", function(docs){
              console.log(docs);

              db.dropCollection("dishes", function(result){
                console.log(result);

                db.close();
              });
            });
          });
      });
    });
  });
