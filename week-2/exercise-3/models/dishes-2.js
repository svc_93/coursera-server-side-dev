// Understanding Mongoose and DB Models for MongoDB - Embedded Document Schema

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// add comment schema
var commentSchema = new Schema({
  rating: {
    type: Number,
    min: 1,
    max: 5,
    required: true
  },
  comment: {
    type: String,
    required: true
  },
  author: {
    type: String,
    required: true
  }
},{
  timestamps: true
});

// create a Schema
var dishSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  description: {
    type: String,
    required: true
  },
  comments: [commentSchema]
}, {
  // automatically adds createdAt and updatedAt values to the documents
  timestamps: true
});

// create a Model for the above schema
// Mongoose creates a collection in MongoDB with the plural of the model name specified here
var Dishes = mongoose.model('Dish', dishSchema);

// make this model available to the Node application
module.exports = Dishes;
