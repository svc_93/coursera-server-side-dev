// Understanding Mongoose and DB Models for MongoDB - Simple Schema

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a Schema
var dishSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  description: {
    type: String,
    required: true
  }
}, {
  // automatically adds createdAt and updatedAt values to the documents
  timestamps: true
});

// create a Model for the above schema
// Mongoose creates a collection in MongoDB with the plural of the model name specified here
var Dishes = mongoose.model('Dish', dishSchema);

// make this model available to the Node application
module.exports = Dishes;
