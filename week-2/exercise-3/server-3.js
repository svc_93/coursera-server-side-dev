// Using Mongoose Models - Adding Comments into Dishes (subdocuments)

var mongoose = require('mongoose'),
  assert = require('assert');

var Dishes = require('./models/dishes-2');

// Connection URL
var url = 'mongodb://localhost:27017/conFusion';
mongoose.connect(url);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function(){
  // Connected to DB
  console.log("Connected correctly to the server");

  // create a new dish
  Dishes.create({
    name: 'Uthapizza',
    description: 'Test',
    comments: [
      {
        rating: 3,
        comment: 'This is insane',
        author: 'Matt Daemon'
      }
    ]
  }, function(err, dish){
    if(err) throw err;

    console.log('Dish created!');
    console.log(dish);
    var id = dish._id;

    // get all the dishes - 3 sec timeout between create and update
    setTimeout(function(){
      Dishes.findByIdAndUpdate(id, {
        $set: {
          description: 'Updated Test'
        }
      }, {
        // new = true returns updated dish
        new: true
      })
      .exec(function(err,dish){
        if(err) throw err;
        console.log('Updated Dish!');
        console.log(dish);

        // Adding new comments
        dish.comments.push({
          rating: 4,
          comment: 'I\'m getting a sinking feeling',
          author: 'Leo'
        });

        dish.save(function(err,dish){
          console.log('Updated comments!');
          console.log(dish);

          db.collection('dishes').drop(function(result){
            db.close();
          });
        });
      });
    },3000);
  });
});
