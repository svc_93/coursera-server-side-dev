// Using Mongoose Models - Create a dish

var mongoose = require('mongoose'),
  assert = require('assert');

var Dishes = require('./models/dishes-1');

// Connection URL
var url = 'mongodb://localhost:27017/conFusion';
mongoose.connect(url);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function(){
  // Connected to DB
  console.log("Connected correctly to the server");

  // create a new dish
  var newDish = Dishes({
    name: 'Klingon',
    description: 'Test'
  });

  // save the dish
  newDish.save(function(err){
    if(err) throw err;

    console.log('Dish created');

    // find all the dishes
    Dishes.find({}, function(err, dishes){
      if(err) throw err;

      // object of all the users
      console.log(dishes);

      db.collection('dishes').drop(function(result){
        db.close();
      });
    });
  });
});
