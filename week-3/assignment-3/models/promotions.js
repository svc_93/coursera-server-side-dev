// Mongoose Model for Promotions - mongoose-currency and default values

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Add currency to Mongoose Schema Types
require('mongoose-currency').loadType(mongoose);
var Currency = mongoose.Types.Currency;

// create a Schema
var promoSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  image: {
    type: String,
    required: true,
  },
  label: {
    type: String,
    default: ""
  },
  price: {
    type: Currency,
    required: true
  },
  description: {
    type: String,
    required: true
  }
}, {
  // automatically adds createdAt and updatedAt values to the documents
  timestamps: true
});

// create a Model for the above schema
// Mongoose creates a collection in MongoDB with the plural of the model name specified here
var Promotions = mongoose.model('Promotion', promoSchema);

// make this model available to the Node application
module.exports = Promotions;
