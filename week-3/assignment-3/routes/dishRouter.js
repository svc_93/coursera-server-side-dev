// dishRouter is a mini-express module for all dishes
// Connected to mongoDB using mongoose
// Handling subdocuments - comments

var express = require('express');
var mongoose = require('mongoose');

var Dishes = require('../models/dishes');

// Verify module to identify user access rights
var Verify = require('./verify');

var dishRouter = express.Router();

// Verify user before granting access to each route
dishRouter.route('/')
.get(Verify.verifyOrdinaryUser, function(req,res,next){
  Dishes.find({}, function(err, dish){
    if(err) throw err;
    res.json(dish);
  });
})

.post(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req,res,next){
  Dishes.create(req.body, function(err, dish){
    if (err) throw err;
    console.log('Dish created!');
    var id = dish._id;

    res.writeHead(200, {
      'Content-Type': 'text/plain'
    });
    res.end('Added the dish with id: '+ id);
  });
})

.delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req,res,next){
  Dishes.remove({}, function(err,resp){
    if(err) throw err;
    res.json(resp);
  });
});

// For each particular dish, apply following HTTP methods
dishRouter.route('/:dishId')
.get(Verify.verifyOrdinaryUser, function(req,res,next){
  Dishes.findById(req.params.dishId, function(err, dish){
    if(err) throw err;
    res.json(dish);
  });
})

.put(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req,res,next){
  Dishes.findByIdAndUpdate(req.params.dishId, {
    $set: req.body
  }, {
    new: true
  }, function(err,dish){
    if(err) throw err;
    res.json(dish);
  });
})

.delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req,res,next){
  Dishes.findByIdAndRemove(req.params.dishId, function(err, resp){
    if(err) throw err;
    res.json(resp);
  });
});

// Handling subdocuments - All comments
dishRouter.route('/:dishId/comments')
.get(Verify.verifyOrdinaryUser, function(req,res,next){
  Dishes.findById(req.params.dishId, function(err,dish){
    if(err) throw err;
    res.json(dish.comments);
  });
})

.post(Verify.verifyOrdinaryUser, function(req,res,next){
  Dishes.findById(req.params.dishId, function(err, dish){
    if(err) throw err;
    dish.comments.push(req.body);
    dish.save(function(err, dish){
      if(err) throw err;
      console.log('Updated Comments');
      res.json(dish);
    });
  });
})

// Delete for array is not available directly in mongoose
.delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req,res,next){
  Dishes.findById(req.params.dishId, function(err, dish){
    if(err) throw err;
    for(var i = (dish.comments.length - 1); i>=0; i--){
      dish.comments.id(dish.comments[i]._id).remove();
    }
    dish.save(function(err,result){
      if(err) throw err;
      res.writeHead (200, {
        'Content-Type': 'text/plain'
      });
      res.end('Deleted all comments');
    });
  });
});

// Handling subdocuments - for each comment
dishRouter.route('/:dishId/comments/:commentId')
.get(Verify.verifyOrdinaryUser, function(req,res,next){
  Dishes.findById(req.params.dishId, function(err, dish){
    if(err) throw err;
    res.json(dish.comments.id(req.params.commentId));
  });
})

// Cannot update comment inside comment array directly
// So we delete the existing comment and insert the updated comment as a new comment
.put(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req,res,next){
  Dishes.findById(req.params.dishId, function(err,dish) {
    if(err) throw err;
    dish.comments.id(req.params.commentId).remove();
    dish.comments.push(req.body);
    dish.save(function(err, dish){
      if(err) throw err;
      console.log('Updated comments!');
      res.json(dish);
    });
  });
})

.delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req,res,next){
  Dishes.findById(req.params.dishId, function(err, dish){
    dish.comments.id(req.params.commentId).remove();
    dish.save(function(err, resp){
      if(err) throw err;
      res.json(resp);
    });
  });
});


module.exports = dishRouter;
