// Module to supply and verify JSON Web Tokens to the app
//  include this verify module in all routes that need to be access-controlled
//  middleware jsonwebtoken used to create, sign, and verify tokens

var User = require('../models/user');
var jwt = require('jsonwebtoken');

var config = require('../config');

// generate web token for user - lifetime of 3600 seconds
exports.getToken = function(user){
  return jwt.sign(user, config.secretKey, {
      expiresIn: 3600
  });
};

// verify web token of user
exports.verifyOrdinaryUser = function(req, res, next){
  // check header or url params or post params for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  // decode token
  if(token){

    // verifies secret and checks exp
    //  .verify is a jwt in-built method
    //  payload content is available in req.decoded
    jwt.verify(token, config.secretKey, function(err, decoded){
      if(err){
        var err = new Error('You are not authenticated');
        err.status = 401;
        return next(err);
      } else {
        // if everything is swell, save to request for use in other routes
        req.decoded = decoded;
        next();
      }
    });
  } else {
      // if there is no token, return err
      var err = new Error('No token provided!');
      err.status = 403;
      return next(err);
  }
};

// verify if user is an admin
exports.verifyAdmin = function (req,res,next){
  if(req.decoded._doc.admin){
    // user is an admin
    next();
  } else {
    // user is not an admin
    var err = new Error('You do not have sufficient privileges');
    err.status = 403;
    return next(err);
  }
};
