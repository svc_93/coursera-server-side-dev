// leaderRouter is a mini-express module for all leaders
// Connected to mongoDB using mongoose

var express = require('express');
var mongoose = require('mongoose');

var Leadership = require('../models/leadership');

// Verify module to identify user access rights
var Verify = require('./verify');

var leaderRouter = express.Router();

// Verify user before granting access to each route
leaderRouter.route('/')
// all function is no longer required - status is returned corresponding to action
.get(Verify.verifyOrdinaryUser, function(req,res,next){
  Leadership.find({}, function(err, leader){
    if(err) throw err;
    res.json(leader);
  });
})

.post(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req,res,next){
  Leadership.create(req.body, function(err, leader){
    if (err) throw err;
    console.log('Leader created!');
    var id = leader._id;

    res.writeHead(200, {
      'Content-Type': 'text/plain'
    });
    res.end('Added the leader with id: '+ id);
  });
})

.delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req,res,next){
  Leadership.remove({}, function(err,resp){
    if(err) throw err;
    res.json(resp);
  });
});

// For each particular leader, apply following HTTP methods
leaderRouter.route('/:leaderId')
.get(Verify.verifyOrdinaryUser, function(req,res,next){
  Leadership.findById(req.params.leaderId, function(err, leader){
    if(err) throw err;
    res.json(leader);
  });
})

.put(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req,res,next){
  Leadership.findByIdAndUpdate(req.params.leaderId, {
    $set: req.body
  }, {
    new: true
  }, function(err,leader){
    if(err) throw err;
    res.json(leader);
  });
})

.delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req,res,next){
  Leadership.findByIdAndRemove(req.params.leaderId, function(err, resp){
    if(err) throw err;
    res.json(resp);
  });
});

module.exports = leaderRouter;
