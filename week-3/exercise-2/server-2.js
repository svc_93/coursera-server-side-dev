// Using Signed Cookies

var express  = require('express');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');

var hostname = 'localhost';
var port = 3000;

var app = express();

app.use(morgan('dev'));

// secret key for signing cookies
app.use(cookieParser('12345-67890-09876-54321'));

// Auth middleware
function auth(req,res, next){
  console.log(req.headers);

  if(!req.signedCookies.user){
    var authHeader = req.headers.authorization;
    if (!authHeader){
      var err = new Error('You are not authenticated!');
      err.status = 401;
      next(err);
      return;
    }
    // "username:password" is encoded using base64 format - decode correspondingly
    // split authHeader into 'Basic ' and 'username':'password'
    var auth = new Buffer(authHeader.split(' ')[1], 'base64').toString().split(':');
    var user = auth[0];
    var pass = auth[1];

    if(user=='admin' && pass == 'pass'){
      // set cookie upon first request
      res.cookie('user','admin',{signed:true});
      next();
    } else {
      var err = new Error('You are not authenticated!');
      err.status = 401;
      next(err);
    }
  } else {
    if(req.signedCookies.user === 'admin'){
      console.log(req.signedCookies);
      next();
    } else {
      var err = new Error('You are not authenticated');
      err.status = 401;
      next(err);
    }
  }
}

app.use(auth);

app.use(express.static(__dirname + '/public'));

// middleware function to handle auth errors
app.use(function(err, req, res, next){
  res.writeHead(err.status || 500, {
    'WWW-Authenticate': 'Basic',
    'Content-Type': 'text/plain'
  });
  res.end(err.message);
});

app.listen(port, hostname, function(){
  // Console magic - read variables with backquotes
  console.log(`Server running at http://${hostname}:${port}/`);
});
