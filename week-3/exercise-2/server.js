// Basic Authentication using Express

var express  = require('express');
var morgan = require('morgan');

var hostname = 'localhost';
var port = 3000;

var app = express();

app.use(morgan('dev'));

// Auth middleware
function auth(req,res, next){
  console.log(req.headers);

  var authHeader = req.headers.authorization;
  if (!authHeader){
    var err = new Error('You are not authenticated!');
    err.status = 401;
    next(err);
    return;
  }
  // "username:password" is encoded using base64 format - decode correspondingly
  // split authHeader into 'Basic ' and 'username':'password'
  var auth = new Buffer(authHeader.split(' ')[1], 'base64').toString().split(':');
  var user = auth[0];
  var pass = auth[1];

  if(user=='admin' && pass == 'password'){
    next();
  } else {
    var err = new Error('You are not authenticated!');
    err.status = 401;
    next(err);
  }
}

app.use(auth);

app.use(express.static(__dirname + '/public'));

// middleware function to handle auth errors
app.use(function(err, req, res, next){
  res.writeHead(err.status || 500, {
    'WWW-Authenticate': 'Basic',
    'Content-Type': 'text/plain'
  });
  res.end(err.message);
});

app.listen(port, hostname, function(){
  // Console magic - read variables with backquotes
  console.log(`Server running at http://${hostname}:${port}/`);
});
