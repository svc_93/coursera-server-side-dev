// Understanding mongoose population - reference across documents
// dishRouter is a mini-express module for all dishes
//
// Optimised verify by adding .all for all routes
// Individual comments are allowed to be deleted only by the user who posted it
//

var express = require('express');
var mongoose = require('mongoose');

var Dishes = require('../models/dishes');

// Verify module to identify user access rights
var Verify = require('./verify');

var dishRouter = express.Router();

// Verify user before granting access to each route
dishRouter.route('/')
.all(Verify.verifyOrdinaryUser)

.get(function(req,res,next){
  // Populate user info for comments
  Dishes.find({})
  .populate('comments.postedBy')
  .exec(function(err, dish){
    if(err) throw err;
    res.json(dish);
  });
})

.post(Verify.verifyAdmin, function(req,res,next){
  Dishes.create(req.body, function(err, dish){
    if (err) throw err;
    console.log('Dish created!');
    var id = dish._id;

    res.writeHead(200, {
      'Content-Type': 'text/plain'
    });
    res.end('Added the dish with id: '+ id);
  });
})

.delete(Verify.verifyAdmin, function(req,res,next){
  Dishes.remove({}, function(err,resp){
    if(err) throw err;
    res.json(resp);
  });
});

// Verify user before granting access to each route
// For each particular dish, apply following HTTP methods
dishRouter.route('/:dishId')
.all(Verify.verifyOrdinaryUser)

.get(function(req,res,next){
  // Populate user info for each comment
  Dishes.findById(req.params.dishId)
  .populate('comments.postedBy')
  .exec(function(err, dish){
    if(err) throw err;
    res.json(dish);
  });
})

.put(Verify.verifyAdmin, function(req,res,next){
  Dishes.findByIdAndUpdate(req.params.dishId, {
    $set: req.body
  }, {
    new: true
  }, function(err,dish){
    if(err) throw err;
    res.json(dish);
  });
})

.delete(Verify.verifyAdmin, function(req,res,next){
  Dishes.findByIdAndRemove(req.params.dishId, function(err, resp){
    if(err) throw err;
    res.json(resp);
  });
});

// Handling comments
dishRouter.route('/:dishId/comments')
.all(Verify.verifyOrdinaryUser)

.get(function(req,res,next){
  // Populate user info for comments
  Dishes.findById(req.params.dishId)
  .populate('comments.postedBy')
  .exec(function(err,dish){
    if(err) throw err;
    res.json(dish.comments);
  });
})

.post(function(req,res,next){
  Dishes.findById(req.params.dishId, function(err, dish){
    if(err) throw err;

    // record id of user into postedBy property
    req.body.postedBy = req.decoded._doc._id;

    dish.comments.push(req.body);
    dish.save(function(err, dish){
      if(err) throw err;
      console.log('Updated Comments');
      res.json(dish);
    });
  });
})

// Comments can be deleted only by admin
.delete(Verify.verifyAdmin, function(req,res,next){
  Dishes.findById(req.params.dishId, function(err, dish){
    if(err) throw err;
    for(var i = (dish.comments.length - 1); i>=0; i--){
      dish.comments.id(dish.comments[i]._id).remove();
    }
    dish.save(function(err,result){
      if(err) throw err;
      res.writeHead (200, {
        'Content-Type': 'text/plain'
      });
      res.end('Deleted all comments');
    });
  });
});

// Handling each comment
dishRouter.route('/:dishId/comments/:commentId')
.all(Verify.verifyOrdinaryUser)

.get(function(req,res,next){
  // Populate user info for each comment
  Dishes.findById(req.params.dishId)
  .populate('comments.postedBy')
  .exec(function(err, dish){
    if(err) throw err;
    res.json(dish.comments.id(req.params.commentId));
  });
})

// Comment can be updated only by corresponding user
.put(function(req,res,next){
  Dishes.findById(req.params.dishId, function(err,dish) {
    if(err) throw err;
    dish.comments.id(req.params.commentId).remove();

    // record id of user into postedBy property
    req.body.postedBy = req.decoded._doc._id;

    dish.comments.push(req.body);
    dish.save(function(err, dish){
      if(err) throw err;
      console.log('Updated comments!');
      res.json(dish);
    });
  });
})

// comment can be deleted only by the user who posted it
.delete(function(req,res,next){
  Dishes.findById(req.params.dishId, function(err, dish){
    // verify user id before deleting comment
    if(dish.comments.id(req.params.commentId).postedBy != req.decoded._doc._id){
      var err = new Error('You are not authorised to perform this operation');
      err.status = 403;
      return next(err);
    }

    dish.comments.id(req.params.commentId).remove();
    dish.save(function(err, resp){
      if(err) throw err;
      res.json(resp);
    });
  });
});


module.exports = dishRouter;
