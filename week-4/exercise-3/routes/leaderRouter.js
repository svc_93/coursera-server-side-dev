// leaderRouter is a mini-express module for all leaders
// Connected to mongoDB using mongoose
// Optimised verify by adding .all for all routes

var express = require('express');
var mongoose = require('mongoose');

var Leadership = require('../models/leadership');

// Verify module to identify user access rights
var Verify = require('./verify');

var leaderRouter = express.Router();

// Verify user before granting access to each route
leaderRouter.route('/')
.all(Verify.verifyOrdinaryUser)

.get(function(req,res,next){
  Leadership.find({}, function(err, leader){
    if(err) throw err;
    res.json(leader);
  });
})

.post(Verify.verifyAdmin, function(req,res,next){
  Leadership.create(req.body, function(err, leader){
    if (err) throw err;
    console.log('Leader created!');
    var id = leader._id;

    res.writeHead(200, {
      'Content-Type': 'text/plain'
    });
    res.end('Added the leader with id: '+ id);
  });
})

.delete(Verify.verifyAdmin, function(req,res,next){
  Leadership.remove({}, function(err,resp){
    if(err) throw err;
    res.json(resp);
  });
});

// For each particular leader, apply following HTTP methods
leaderRouter.route('/:leaderId')
.all(Verify.verifyOrdinaryUser)

.get(function(req,res,next){
  Leadership.findById(req.params.leaderId, function(err, leader){
    if(err) throw err;
    res.json(leader);
  });
})

.put(Verify.verifyAdmin, function(req,res,next){
  Leadership.findByIdAndUpdate(req.params.leaderId, {
    $set: req.body
  }, {
    new: true
  }, function(err,leader){
    if(err) throw err;
    res.json(leader);
  });
})

.delete(Verify.verifyAdmin, function(req,res,next){
  Leadership.findByIdAndRemove(req.params.leaderId, function(err, resp){
    if(err) throw err;
    res.json(resp);
  });
});

module.exports = leaderRouter;
