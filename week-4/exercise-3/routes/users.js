// Route for Users - Authentication using passport and Verify modules

var express = require('express');
var router = express.Router();
var passport = require('passport');
var User = require('../models/user');
var Verify = require('./verify');

/* GET users listing. */
router.get('/', Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req, res, next) {
  // return all users
  User.find({}, function(err, user){
    if(err) throw err;
    res.json(user);
  });
});

// Redirect to facebook for Authentication
router.get('/facebook', passport.authenticate('facebook'), function(req,res){});

// Authentication response (callback) from facebook's url - add user to local db
router.get('/facebook/callback', function(req,res,next){
  passport.authenticate('facebook', function(err, user, info){
    if(err){
      return next(err);
    }
    if(!user){
      return res.status(401).json({
        err: info
      });
    }
    req.logIn(user, function(err){
      if(err){
        return res.status(500).json({
          err: 'Could not log in user'
        });
      }
      var token = Verify.getToken(user);
      res.status(200).json({
        status: 'Login successful!',
        success: true,
        token: token
      });
    });
  })(req,res,next);
});

// Delete all users
router.delete('/reset', Verify.verifyOrdinaryUser, Verify.verifyAdmin, function(req,res,next){
  User.remove({}, function(err,resp){
    if(err) throw err;
    res.json(resp);
  });
});

// New user registration - POST request contains body in JSON format
// Passport supports new user registration by <modelName>.register()
router.post('/register', function(req,res){
  User.register(new User({username: req.body.username}),
    req.body.password, function(err, user){
      if(err){
        return res.status(500).json({err: err});
      }

      // Add additional information about user
      if(req.body.firstname){
        user.firstname = req.body.firstname;
      }
      if(req.body.lastname){
        user.lastname = req.body.lastname;
      }

      // Now post the user information to DB
      user.save(function(err,user){
        passport.authenticate('local')(req,res,function(){
          return res.status(200).json({status: 'Registration successful'});
        });
      });
    });
});

// Login of an existing user
// passport supports logIn and logOut on request params
router.post('/login', function(req,res,next){
  passport.authenticate('local', function(err, user, info){
    if(err){
      return next(err);
    }
    if(!user){
      return res.status(401).json({
        err: info
      });
    }
    req.logIn(user, function(err){
      if(err){
        return res.status(500).json({
          err: 'Could not login user'
        });
      }

      console.log('User in users: ', user);

      var token = Verify.getToken(user);

      res.status(200).json({
        status: 'Login successful',
        success: true,
        token: token
      });
    });
  })(req,res,next);
});

// Logout of user - destroy token as well
router.get('/logout', function(req,res){
  req.logout();
  res.status(200).json({
    status: 'Bye!'
  });
});


module.exports = router;
