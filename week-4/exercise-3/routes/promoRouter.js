// promoRouter is a mini-express module for all promotions
// Connected to mongoDB using mongoose
// Optimised verify by adding .all for all routes

var express = require('express');
var mongoose = require('mongoose');

var Promotions = require('../models/promotions');

// Verify module to identify user access rights
var Verify = require('./verify');

var promoRouter = express.Router();

// Verify user before granting access to each route
promoRouter.route('/')
.all(Verify.verifyOrdinaryUser)

.get(function(req,res,next){
  Promotions.find({}, function(err, promotion){
    if(err) throw err;
    res.json(promotion);
  });
})

.post(Verify.verifyAdmin, function(req,res,next){
  Promotions.create(req.body, function(err, promotion){
    if (err) throw err;
    console.log('Promotion created!');
    var id = promotion._id;

    res.writeHead(200, {
      'Content-Type': 'text/plain'
    });
    res.end('Added the promotion with id: '+ id);
  });
})

.delete(Verify.verifyAdmin, function(req,res,next){
  Promotions.remove({}, function(err,resp){
    if(err) throw err;
    res.json(resp);
  });
});

// For each particular promotion, apply following HTTP methods
promoRouter.route('/:promoId')
.all(Verify.verifyOrdinaryUser)

.get(function(req,res,next){
  Promotions.findById(req.params.promoId, function(err, promotion){
    if(err) throw err;
    res.json(promotion);
  });
})

.put(Verify.verifyAdmin, function(req,res,next){
  Promotions.findByIdAndUpdate(req.params.promoId, {
    $set: req.body
  }, {
    new: true
  }, function(err,promotion){
    if(err) throw err;
    res.json(promotion);
  });})

.delete(Verify.verifyAdmin, function(req,res,next){
  Promotions.findByIdAndRemove(req.params.promoId, function(err, resp){
    if(err) throw err;
    res.json(resp);
  });
});

module.exports = promoRouter;
