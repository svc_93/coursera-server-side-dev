// Tracking configuration for Authentication and Mongo

module.exports = {
  'secretKey':'12345-67890-09876-54321',
  'mongoUrl':'mongodb://localhost:27017/conFusion',
  'facebook': {
    clientID: '216816482004058',
    clientSecret: 'c1ea776e251a7327ec93f302facc8620',
    callbackURL: 'https://localhost:3443/users/facebook/callback'
  }
};
