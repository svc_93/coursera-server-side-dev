// Understanding HTTP Methods, File System and Path

var http = require('http');
var fs = require('fs');
var path = require('path');

var hostname = 'localhost';
var port = 8081;

var server = http.createServer(function(req,res){
  console.log('Request for '+ req.url + ' by method '+ req.method);

  if(req.method=='GET'){
    var fileUrl;

    if(req.url=="/") fileUrl = '/index.html';
    else fileUrl = req.url;

    // Handles file path as per operating system
    var filePath = path.resolve('./public'+fileUrl);

    // Extracts file extension
    var fileExt = path.extname(filePath);

    if(fileExt=='.html'){
      fs.exists(filePath, function(exists){
        // exists is a boolean value
        if(!exists){
          res.writeHead(404, {'Content-Type': 'text/html'});
          res.end('<h1>Error 404: ' + fileUrl + 'not found</h1>');
          return;
        }

        // if file exists, then read file stream and pipe to user
          res.writeHead(200,{'Content-Type':'text/html'});
          fs.createReadStream(filePath).pipe(res);
      });
    } else {
      // Not a HTML file
      res.writeHead(404, {'Content-Type': 'text/html'});
      res.end('<h1>Error 404: '+fileUrl+' not a HTML file</h1>');
    }
  } else {
    // Request is not a GET method
    res.writeHead(400,{'Content-Type':'text/html'});
    res.end('<h1>Error 400: ' + req.method + ' not supported</h1>');
  }

})

server.listen(port, hostname, function(){
  console.log("server running at "+hostname+":"+port);
})
