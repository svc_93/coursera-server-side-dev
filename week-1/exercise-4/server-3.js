// Understanding express and middleware
// BodyParser enables the parsing of data in request body, and converts it to JS objects in the request body
//  bodyParser can parse many types of data including json, multipart-forms

var express  = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');

var hostname = 'localhost';
var port = 8081;

var app = express();
app.use(morgan('dev'));
app.use(bodyParser.json());

// all methods, as long as url has /dishes
app.all('/dishes', function(req,res,next){
  res.writeHead(200, {'Content-Type':'text/plain'});
  // call other middleware functions as parsing continues - async programming
  next();
});

// get method
app.get('/dishes', function(req,res,next){
  res.end('Will send all the dishes to you!');
});

// post method
app.post('/dishes', function(req,res,next){
  res.end('Will add dish: '+req.body.name+' with details: '+ req.body.description);
});

// delete method
app.delete('/dishes', function(req,res,next){
  res.end('Deleting all dishes');
});

// using req.params for particular dishId
app.get('/dishes/:dishId', function(req,res,next){
  res.end('Will send details of dish: '+ req.params.dishId+' to you!');
});

// put method for particular dish
app.put('/dishes/:dishId', function(req,res,next){
  res.write('Updating the dish: '+req.params.dishId+'\n');
  res.end('Will update the dish '+req.body.name+' with details: '+req.body.description);
});

// delete method for particular dish
app.delete('/dishes/:dishId', function(req,res,next){
  res.end('Deleting dish: '+ req.params.dishId);
});


app.use(express.static(__dirname + '/public'));

app.listen(port, hostname, function(){
  // Console magic - read variables with backquotes
  console.log(`Server running at http://${hostname}:${port}/`);
})
