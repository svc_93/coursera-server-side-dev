// Understanding express Router
//  express.Router() creates a mini-express module and supports all HTTP methods

var express  = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');

var hostname = 'localhost';
var port = 8081;

var app = express();
app.use(morgan('dev'));

// dishRouter is a mini-express module that can be used for routing
var dishRouter = express.Router();
dishRouter.use(bodyParser.json());
dishRouter.route('/')
.all(function(req,res,next){
  res.writeHead(200, {'Content-Type':'text/plain'});
  next();
})

.get(function(req,res,next){
  res.end('Will send all the dishes to you!');
})

.post(function(req,res,next){
  res.end('Will add dish: '+req.body.name+' with details: '+ req.body.description);
})

.delete(function(req,res,next){
  res.end('Deleting all dishes');
});

// For each particular dish, apply following HTTP methods
dishRouter.route('/:dishId')
.all(function(req,res,next){
  res.writeHead(200, {'Content-Type':'text/plain'});
  next();
})

.get(function(req,res,next){
  res.end('Will send details of dish: '+ req.params.dishId+' to you!');
})

.put(function(req,res,next){
  res.write('Updating the dish: '+req.params.dishId+'\n');
  res.end('Will update the dish '+req.body.name+' with details: '+req.body.description);
})

.delete(function(req,res,next){
  res.end('Deleting dish: '+ req.params.dishId);
});

// if URL contains '/dishes', then apply dishRouter
app.use('/dishes', dishRouter);

app.use(express.static(__dirname + '/public'));

app.listen(port, hostname, function(){
  // Console magic - read variables with backquotes
  console.log(`Server running at http://${hostname}:${port}/`);
})
