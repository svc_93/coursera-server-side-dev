// Understanding express and middleware
// Morgan is used to log information on server-side
// Static is a middleware that is installed by default

var express  = require('express');
var morgan = require('morgan');

var hostname = 'localhost';
var port = 8081;

var app = express();
app.use(morgan('dev'));
app.use(express.static(__dirname + '/public'));

app.listen(port, hostname, function(){
  // Console magic - read variables with backquotes
  console.log(`Server running at http://${hostname}:${port}/`);
})
