// Understanding http and express

var express  = require('express');
    http = require('http');

var hostname = 'localhost';
var port = 8081;

var app = express();

app.use(function(req,res,next){
  console.log(req.headers);
  res.writeHead(200, {'Content-Type':'text/html'});
  res.end('<html><body><h1>Hello Express</h1></body></html>');
});

var server = http.createServer(app);

server.listen(port, hostname, function(){
  // Console magic - read variables with backquotes
  console.log(`Server running at http://${hostname}:${port}/`);
})
