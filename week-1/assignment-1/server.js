// Understanding express Router and require modules
//  express.Router() creates a mini-express module and supports all HTTP methods

var express  = require('express');
var morgan = require('morgan');
var dishRouter = require('./dishRouter');
var promoRouter = require('./promoRouter');
var leaderRouter = require('./leaderRouter');

var hostname = 'localhost';
var port = 8081;

var app = express();
app.use(morgan('dev'));

// redirect all URL requests to the proper Router
app.use('/dishes', dishRouter);
app.use('/promotions', promoRouter);
app.use('/leadership', leaderRouter);

app.use(express.static(__dirname + '/public'));

app.listen(port, hostname, function(){
  // Console magic - read variables with backquotes
  console.log(`Server running at http://${hostname}:${port}/`);
})
